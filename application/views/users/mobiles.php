<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Learn CURD Operation On Web Preparations</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/buttons.bootstrap.min.css')?>" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
      <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/dataTables.bootstrap.js')?>"></script>
  </head>
  <body>
  <div class="container">
    <h1>CURD CodeIgniter Framework with AJAX and Bootstrap</h1>
</center>
    <br />
    <button class="btn btn-success" onclick="add_mobile()"><i class="glyphicon glyphicon-plus"></i> Add</button>
    <a href="<?php echo base_url(); ?>index.php/user/logout" class="btn btn-danger">Logout</a>
    <br />
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
       <tr>
          <th>User_id</th>
          <th>User_firstname</th>
          <th>User_lastname</th>
          <th>User_email</th>
          <th>User_phone</th>
          <th style="width:125px;">Action
          </p></th>
        </tr>
      </thead>
      <tbody>
				<?php foreach($mobiles as $mobile){?>
				     <tr>
				         <td><?php echo $mobile->user_id;?></td>
				         <td><?php echo $mobile->user_firstname;?></td>
								 <td><?php echo $mobile->user_lastname;?></td>
								<td><?php echo $mobile->user_email;?></td>
								<td><?php echo $mobile->user_phone;?></td>
								<td>
									<button class="btn btn-warning" onclick="edit_mobile(<?php echo $mobile->user_id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
									<button class="btn btn-danger" onclick="delete_mobile(<?php echo $mobile->user_id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
								</td>
				      </tr>
				     <?php }?>
      </tbody>
 
      <tfoot>
        <tr>
          <th>User_id</th>
          <th>User_firstname</th>
          <th>User_lastname</th>
          <th>User_email</th>
          <th>User_phone</th>
          <th>Action</th>
        </tr>
      </tfoot>
    </table>
  </div> 
 
  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
 
    function add_mobile()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    function edit_mobile(user_id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-ground').removeClass('has-error');
      $('.help-block').empty();
 
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('mobiles/ajax_edit/')?>/" + user_id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="user_id"]').val(data.user_id);
            $('[name="user_firstname"]').val(data.user_firstname);
            $('[name="user_lastname"]').val(data.user_lastname);
            $('[name="user_email"]').val(data.user_email);
            $('[name="user_phone"]').val(data.user_phone);
 
 
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Mobile'); // Set title to Bootstrap modal title
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
 
 
 
    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('mobiles/mobile_add')?>";
      }
      else
      {
        url = "<?php echo site_url('mobiles/mobile_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
 
    function delete_mobile(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('mobiles/mobile_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
      }
    }
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title alert alert-info">Mobile Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="user_id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">User_firstname</label>
              <div class="col-md-9">
                <input name="user_firstname" placeholder="User_firstname" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">User_lastname</label>
              <div class="col-md-9">
                <input name="user_lastname" placeholder="User_lastname" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">User_email</label>
              <div class="col-md-9">
								<input name="user_email" placeholder="User_email" class="form-control" type="text">
 
              </div>
            </div>
						<div class="form-group">
							<label class="control-label col-md-3">User_phone</label>
							<div class="col-md-9">
								<input name="user_phone" placeholder="User_phone" class="form-control" type="text">
 
							</div>
			</div>
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
 
  </body>
</html>