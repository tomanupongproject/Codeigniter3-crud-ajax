<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Book Model for CodeIgniter CRUD using Ajax Application.
class Mobile extends CI_Model
{
	var $table = 'ex_user';
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all_mobiles()
	{
		$this->db->from('ex_user');
		$query=$this->db->get();
		return $query->result();
	}
 
	public function get_by_id($user_id)
	{
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
 
		return $query->row();
	}
 
	public function mobile_add($data)
	{   //print_r($data);
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
 
	public function mobile_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
 
	public function delete_by_id($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete($this->table);
	}
	public function login($username, $password){
		$query = $this->db->get_where('ex_user', array('username'=>$username, 'password'=>$password));
		return $query->row_array();
	}
}