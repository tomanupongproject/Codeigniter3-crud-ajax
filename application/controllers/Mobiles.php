<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Controller for codeigniter crud using ajax application.
class Mobiles extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mobile');
	}
	public function index(){
		//load session library
		$this->load->library('session');
 
		//restrict users to go back to login if session has been set
		if($this->session->userdata('user')){
			redirect('Mobiles/showlist');
		}
		else{
			$this->load->view('login_page');
		}
	}
	public function showlist()
	{
		$data['mobiles']=$this->mobile->get_all_mobiles();
		$this->load->view('users/header');
		$this->load->view('users/mobiles',$data);
		$this->load->view('users/footer');
	}
	public function mobile_add()
	{
		$data = array(
				'user_firstname' => $this->input->post('user_firstname'),
				'user_lastname' => $this->input->post('user_lastname'),
				'user_email' => $this->input->post('user_email'),
				'user_phone' => $this->input->post('user_phone'),
			);
		$insert = $this->mobile->mobile_add($data);
		echo json_encode(array("status" => TRUE));
	}
	public function ajax_edit($user_id)
	{
		$data = $this->mobile->get_by_id($user_id);
		echo json_encode($data);
	}
	
	public function mobile_update()
	{
		$data = array(
				'user_firstname' => $this->input->post('user_firstname'),
				'user_lastname' => $this->input->post('user_lastname'),
				'user_email' => $this->input->post('user_email'),
				'user_phone' => $this->input->post('user_phone'),
			);
		$this->mobile->mobile_update(array('user_id' => $this->input->post('user_id')), $data);
		echo json_encode(array("status" => TRUE));
	}
 
	public function mobile_delete($user_id)
	{
		$this->mobile->delete_by_id($user_id);
		echo json_encode(array("status" => TRUE));
	}
 
	public function login(){
		//load session library
		$this->load->library('session');
 
		$output = array('error' => false);
 
		$username = $_POST['username'];
		$password = $_POST['password'];
 
		$data = $this->users_model->login($username, $password);
 
		if($data){
			$this->session->set_userdata('user', $data);
			$output['message'] = 'Logging in. Please wait...';
		}
		else{
			$output['error'] = true;
			$output['message'] = 'Login Invalid. User not found';
		}
 
		echo json_encode($output); 
	}
 
	public function home(){
		//load session library
		$this->load->library('session');
 
		//restrict users to go to home if not logged in
		if($this->session->userdata('user')){
			$this->load->view('home');
		}
		else{
			redirect('/');
		}
 
	}
 
	public function logout(){
		//load session library
		$this->load->library('session');
		$this->session->unset_userdata('user');
		redirect('/');
	}
}